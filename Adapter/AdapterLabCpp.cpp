// Purpose.  Adapter design pattern lab


#include <iostream>

struct StackStruct {
   int*  array;
   int   sp;
   int   size;
};
typedef StackStruct Stack;

static void initialize( Stack* s, int size ) {
    s->array = new int[size];
    s->size = size;
    s->sp = 0;
}

static void cleanUp( Stack* s ) { delete s->array; }

static int isEmpty( Stack* s ) { return s->sp == 0 ? 1 : 0; }

static int isFull( Stack* s ) { return s->sp == s->size ? 1 : 0; }

static void push( Stack* s, int item ) { if ( ! isFull(s)) s->array[s->sp++] = item; }

static int pop( Stack* s )
{
    if (isEmpty(s)) 
        return 0;
    else
        return s->array[--s->sp]; 
}

class Queue
{
    Stack * stack_;
public:
    Queue(int number)
    {
        stack_ = new Stack;
        initialize(stack_, number);
    }
    ~Queue()
    {
        ::cleanUp(stack_);
        delete stack_;
    }
    int isEmpty() const
    {
        return ::isEmpty(stack_);
    }
    int isFull() const
    {
        return ::isFull(stack_);
    }
    void enque(int elem) {
        ::push(stack_, elem);
    }
    int deque()
    {
        Stack * tmp = new Stack;
        ::initialize(tmp, stack_->size);
        while(! ::isEmpty(stack_))
        {
            ::push(tmp, ::pop(stack_));
        }
        int ret = ::pop(tmp);
        while(! ::isEmpty(tmp))
        {
            ::push(stack_, ::pop(tmp));
        }
        ::cleanUp(tmp);
        return ret;
    }
};

int main() {
    Queue  queue(15);

    for( int i = 0; i < 25; i++ )
        queue.enque( i );
    while( ! queue.isEmpty() )
        std::cout << queue.deque() << " ";
    std::cout << std::endl;
    return 0;
}

// 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 
